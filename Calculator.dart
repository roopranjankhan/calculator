import 'package:flutter/material.dart';

void main() {
  runApp(const Calculator());
}

class Calculator extends StatefulWidget {
  const Calculator({Key? key}) : super(key: key);

  @override
  State<Calculator> createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {

  TextEditingController firstController = TextEditingController();
  TextEditingController secondController = TextEditingController();
  int endResult = 0, firstNumber = 0, secondNumber = 0;

  int addTwoNumber(){
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber + secondNumber;
    });
    return endResult;
  }

  int subTwoNumber(){
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber - secondNumber;
    });
    return endResult;
  }

  int mulTwoNumber(){
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber * secondNumber;
    });
    return endResult;
  }

  int divTwoNumber(){
    setState(() {
      firstNumber = int.parse(firstController.text);
      secondNumber = int.parse(secondController.text);
      endResult = firstNumber ~/ secondNumber;
    });
    return endResult;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Calculator'),
      ),
      body: Padding(
          padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Text(
              "Result : ",
              style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: firstController,
              decoration: InputDecoration(
                labelText: "First Number : ",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20))),
                ),
        SizedBox(
          height: 20,
        ),
        TextField(
          controller: secondController,
          decoration: InputDecoration(
              labelText: "Second Number : ",
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20))),
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          children: [
            ElevatedButton(onPressed: (){
              addTwoNumber();
              firstController.clear();
              secondController.clear();
            }, child: Text("Add")),
            ElevatedButton(onPressed: (){
              subTwoNumber();
              firstController.clear();
              secondController.clear();
            }, child: Text("Subtract")),

          ],
        ),
          Row(
            children: [
              ElevatedButton(onPressed: (){
                mulTwoNumber();
                firstController.clear();
                secondController.clear();
              }, child: Text("Multiply")),
              ElevatedButton(onPressed: (){
                divTwoNumber();
                firstController.clear();
                secondController.clear();
              }, child: Text("Divide")),

            ],
          )
              ),
            ),
          ],
        ),
      ),
        
      );


  }
}

